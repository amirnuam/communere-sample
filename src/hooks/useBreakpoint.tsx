import { useState, useEffect } from "react";
import throttle from "lodash.throttle";

// I Implement this hook to handle responsiveness of app but this subject is out of concern due to the description of project .
const getDeviceConfig = (width: number) => {
  if (width < 320) {
    return { xs: true, sm: false, md: false, lg: false };
  } else if (width >= 320 && width < 720) {
    return { xs: false, sm: true, md: false, lg: false };
  } else if (width >= 720 && width < 1024) {
    return { xs: false, sm: false, md: true, lg: false };
  } else if (width >= 1024) {
    return { xs: false, sm: false, md: false, lg: true };
  }
};

const useBreakpoint = () => {
  const [brkPnt, setBrkPnt] = useState(() =>
    getDeviceConfig(window.innerWidth)
  );

  useEffect(() => {
    const calcInnerWidth = throttle(function () {
      setBrkPnt(getDeviceConfig(window.innerWidth));
    }, 200);
    window.addEventListener("resize", calcInnerWidth);
    return () => window.removeEventListener("resize", calcInnerWidth);
  }, []);

  return brkPnt;
};
export default useBreakpoint;
