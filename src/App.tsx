import LayerContextProvider from "./context/layerContext";
import Main from "./pages/Main";

function App() {
  return (
    <LayerContextProvider>
      <Main />
    </LayerContextProvider>
  );
}

export default App;
