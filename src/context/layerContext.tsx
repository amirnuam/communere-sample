import React, { useContext, useState } from "react";
import { IPointsContext } from "../constants/types";

const LayerContext: any = React.createContext<Partial<IPointsContext>>({
  points: [],
  setPoints: () => {},
});
const LayerContextProvider = ({ children }: any) => {
  const [points, setPoints] = useState<number[][]>([]);
  const [singlePoint, setSinglePoint] = useState<number[]>([]);

  return (
    <LayerContext.Provider
      value={{
        setPoints,
        points,
        setSinglePoint,
        singlePoint,
      }}
    >
      {children}
    </LayerContext.Provider>
  );
};
const usePointsState = (): any => useContext(LayerContext);
const usePointsActions = () => {
  const { setPoints, points, setSinglePoint } = usePointsState();

  const addPoints = (point: any) => {
    setPoints([...points, point]);
  };

  const addSinglePoint = (point: any) => {
    setSinglePoint(point);
  };

  return { addPoints, addSinglePoint };
};

export { usePointsState, usePointsActions };
export default LayerContextProvider;
