import * as React from "react";

interface Props {
  width?: number | string;
  height?: number | string;
  borderColor?: string;
  placeholder?: string;
  style?: object;
  value?: any;
  setValue?(e: any): void;
}

export const MInput: React.FC<Props> = ({
  width = "100%",
  height = "1rem",
  borderColor = "green",
  placeholder = "write your own",
  style = {},
  value = null,
  setValue = () => {},
}) => {
  return (
    <input
      placeholder={placeholder}
      style={{
        width: width,
        height: height,
        border: `2px solid ${borderColor}`,
        margin: "1rem",
        borderRadius: 10,
        ...style,
      }}
      value={value}
      onChange={(e) => setValue(e.target.value)}
    />
  );
};
