import * as React from "react";
import { IItemType } from "../../constants/types";

interface Props {
  items?: IItemType[];
  placeholder?: string;
  style?: object;
  value?: string | number;
  setValue?: (e: any) => void;
}
const MDropdown: React.FC<Props> = ({
  items = [],
  placeholder = "select your own",
  style = {},
  setValue = () => {},
}) => {
  const selectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const value = event.target.value;
    setValue(value);
  };
  return (
    <select
      style={{
        width: "20rem",
        height: "2rem",
        borderRadius: 10,
        border: "1px dashed green",
        margin: "1rem",
        ...style,
      }}
      onChange={selectChange}
    >
      <option value="" disabled selected>
        {placeholder}
      </option>
      {items.length > 0 &&
        items.map((item: IItemType) => (
          <option key={item?.key} value={item?.value}>
            {item?.value}
          </option>
        ))}
    </select>
  );
};

export default MDropdown;
