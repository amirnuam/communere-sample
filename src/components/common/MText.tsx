import * as React from "react";

interface Props {
  title?: string;
  color?: string;
  fontSize?: string | number;
  style?: object;
}

export const MText: React.FC<Props> = ({
  title = "My Text",
  color = "#fff",
  fontSize = "1rem",
  style = {},
}) => {
  return (
    <text style={{ color, fontSize, margin: "1rem", ...style }}>{title}</text>
  );
};
