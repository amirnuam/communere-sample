import * as React from "react";
import { COLORS } from "../../constants/colors";
import { MText } from "./MText";

interface Props {
  width?: number | string;
  height?: number | string;
  backgroundColor?: string;
  borderRadius?: string;
  title?: string;
  style?: object;
  type?: string;
  onSubmit?(): void;
}

export const MButton: React.FC<Props> = ({
  width = "100%",
  height = "2rem",
  backgroundColor = "rgb(99,238,254)",
  borderRadius = "10px",
  title = "Click me",
  style = {},
  type = "primary",
  onSubmit = () => {},
}) => {
  return (
    <button
      style={{
        width: width,
        height: height,
        backgroundColor,
        background: COLORS[type],
        borderRadius,
        margin: "1rem",
        cursor: "pointer",
        ...style,
      }}
      onClick={onSubmit}
    >
      <MText title={title} />
    </button>
  );
};
