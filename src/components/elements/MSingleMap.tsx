import React, { useEffect, useState } from "react";
import {
  LayerGroup,
  MapContainer,
  Marker,
  Popup,
  TileLayer,
  useMapEvents,
} from "react-leaflet";
import { LatLngTuple } from "leaflet";

import { usePointsActions, usePointsState } from "../../context/layerContext";

const defaultLatLng: LatLngTuple = [48.865572, 2.283523];
const zoom: number = 8;

export const MSingleMap: React.FC = () => {
  const { singlePoint } = usePointsState();
  const { addSinglePoint } = usePointsActions();

  const [initialPosition, setInitialPosition] =
    useState<[number, number]>(defaultLatLng);
  const [selectedPosition, setSelectedPosition] = useState<[number, number]>([
    0, 0,
  ]);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      const { latitude, longitude } = position.coords;
      setInitialPosition([latitude, longitude]);
    });
  }, []);

  const Markers = () => {
    useMapEvents({
      click(e) {
        setSelectedPosition([e.latlng.lat, e.latlng.lng]);
        addSinglePoint([e.latlng.lat, e.latlng.lng]);
      },
    });

    return (
      <>
        {singlePoint.length > 0 ? (
          <Marker key={singlePoint[0]} position={singlePoint} interactive>
            <Popup>
              {singlePoint[0]} <br /> {singlePoint[1]}
            </Popup>
          </Marker>
        ) : null}
      </>
    );
  };

  return (
    <MapContainer id="mapId1" center={initialPosition} zoom={zoom}>
      <LayerGroup>{singlePoint}</LayerGroup>
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
      />
      <Markers />
    </MapContainer>
  );
};
