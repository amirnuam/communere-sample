import React, { useState } from "react";
import {
  LayerGroup,
  MapContainer,
  Marker,
  Popup,
  TileLayer,
} from "react-leaflet";
import { LatLngTuple } from "leaflet";
import { usePointsState } from "../../context/layerContext";

const defaultLatLng: LatLngTuple = [48.865572, 2.283523];

const zoom: number = 8;

export const MMap: React.FC = () => {
  const { points } = usePointsState();

  const Markers = () => {
    return (
      <>
        {points.length > 0
          ? points.map((item: any) => (
              <Marker key={item?.point[0]} position={item?.point} interactive>
                <Popup>
                  {item?.title} <br /> {item?.type}
                </Popup>
              </Marker>
            ))
          : null}
      </>
    );
  };

  return (
    <>
      <MapContainer id="mapId" center={defaultLatLng} zoom={zoom}>
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
        />
        <Markers />
      </MapContainer>
    </>
  );
};
