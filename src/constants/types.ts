export type ISinglePointContext = {
  point?: number[];
  title?: string;
  type?: string;
  imgUrl?: string;
};
export type IPointsContext = {
  points?: ISinglePointContext[];
  setPoints?: () => void;
  singlePoint?: number[];
  setSinglePoint?: () => void;
};
export type IItemType = {
  key?: number | string;
  value?: number | string;
};
