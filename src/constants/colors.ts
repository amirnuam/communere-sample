export const COLORS: any = {
  primary:
    "linear-gradient(90deg, rgba(99,238,254,1) 0%, rgba(225,41,246,1) 100%)",
  secondary:
    "linear-gradient(90deg, rgba(99,238,254,1) 0%, rgba(25,2,28,1) 100%)",
};
