import React, { useState } from "react";

import { MButton } from "../../components/common";
import { Add, Show } from "./components";
import classes from "./main.module.css";

const Main: React.FC = () => {
  const [showPoints, setShowPoints] = useState<boolean>(false);

  return (
    <div className={classes.main}>
      <div className={classes.wrapper}>
        <div className={classes.header}>
          <div className={classes.halfCol}>
            <MButton
              title="Show Points"
              width="50%"
              onSubmit={() => setShowPoints(true)}
            />
          </div>
          <div className={classes.halfCol}>
            <MButton
              title="Add Point"
              width="50%"
              onSubmit={() => setShowPoints(false)}
              type="secondary"
            />
          </div>
        </div>
        {showPoints ? <Show /> : <Add />}
      </div>
    </div>
  );
};
export default Main;
