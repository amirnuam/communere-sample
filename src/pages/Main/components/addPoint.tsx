import React, { useState } from "react";

import { MButton, MInput } from "../../../components/common";
import MDropdown from "../../../components/common/MDropdown";
import { MSingleMap } from "./../../../components/elements";
import {
  usePointsActions,
  usePointsState,
} from "../../../context/layerContext";
import classes from "./comp.module.css";

export const Add: React.FC = () => {
  const [selectedvalue, setSelectedValue] = useState<string>("");
  const [title, setTitle] = useState<string>("");

  const { singlePoint, points } = usePointsState();
  const { addPoints } = usePointsActions();

  const addPoint = () => {
    if (singlePoint.length > 0 && (title || selectedvalue)) {
      const data = {
        point: singlePoint,
        title,
        type: selectedvalue,
        imgUrl: "",
      };
      addPoints(data);
      return alert("Successfully done");
    }
    return alert("Please Provide neccessary Info");
  };
  return (
    <div className={classes.addDiv}>
      <MInput
        width="40%"
        height="2rem"
        placeholder="Write your title"
        value={title}
        setValue={setTitle}
      />
      <MSingleMap />
      <MDropdown
        items={[
          { key: 1, value: "Bussiness" },
          { key: 2, value: "Work" },
          { key: 3, value: "Home" },
          { key: 4, value: "Friend House" },
        ]}
        setValue={setSelectedValue}
      />
      <MButton title="Add" width="20%" onSubmit={addPoint} />
      <MButton title="Cancel" width="20%" type="secondary" />
    </div>
  );
};
